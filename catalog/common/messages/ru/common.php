<?php

return [
    'Category' => 'Категория',
    'Slug' => 'ЧПУ',
    'Updated At' => 'Обновлено',
    'Image' => 'Изображение',
    'Status' => 'Статус',
    'Parent Id' => 'Родительская',
    'Gallery Id' => 'Галерея',
    'Quantity' => 'Количество',
    'Pos' => 'Позиция',
    'Actions Ids' => 'Выбрать акцию',
    'Is Recommended' => 'Рекомендовать',
    'Is New' => 'Новый',
    'Is Bestseller' => 'Лучший',
    'Price' => 'Цена',
    'Header' => 'Название',
    'Header2' => 'Заголовок 2',
    'Features' => 'Характеристики',
    'Content' => 'Текст',
    'Short Content' => 'Краткое описание',
];