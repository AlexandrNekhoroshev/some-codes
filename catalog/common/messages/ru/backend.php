<?php

return [
    'Catalog' => 'Каталог',
    'Categories' => 'Категории',
    'Items' => 'Объекты',
    'Sale' => 'Продаваемые номера',
    'Calendar' => 'Календарь',
    'Create category' => 'Создать категорию',
    'Category "{item}" successfully created' => 'Категория "{item}" успешно создана',
    'Category "{item}" successfully updated' => 'Категория "{item}" успешно изменена',
    'Category "{item}" successfully deleted' => 'Категория "{item}" успешно удалена',
    'Item "{item}" successfully created' => 'Объект "{item}" успешно создан',
    'Item "{item}" successfully updated' => 'Объект "{item}" успешно изменен',
    'Item "{item}" successfully deleted' => 'Объект "{item}" успешно удален',
    'Manufacturers' => 'Производители',
    'Collections' => 'Коллекции',
    'Create manufacturer' => 'Добавить производителя',
    'New manufacturer' => 'Новый производитель',
    'Create item' => 'СОЗДАТЬ ЗАЯВКУ',

];