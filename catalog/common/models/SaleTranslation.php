<?php

namespace common\modules\catalog\common\models;

use Yii;
use common\models\Language;
use common\modules\catalog\CatalogModule;

/**
 * This is the model class for table "{{%sale_translation}}".
 *
 * @property integer $item_id
 * @property string $language
 * @property string $header
 * @property string $description
 *
 * @property Language $language0
 * @property Sale $sale
 */
class SaleTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sale_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => CatalogModule::t('common', 'Item'),
            'language' => CatalogModule::t('common', 'Language'),
            'header' => CatalogModule::t('common', 'Header'),
            'description' => CatalogModule::t('common', 'Meta Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sale::className(), ['id' => 'item_id']);
    }
}
