<?php

namespace common\modules\catalog\common\models;

use Yii;
use common\models\Language;
use common\modules\catalog\CatalogModule;

/**
 * This is the model class for table "{{%category_translation}}".
 *
 * @property integer $category_id
 * @property string $language
 * @property string $header
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $mta_title
 * @property string $mta_keywords
 * @property string $mta_description
 *
 * @property Language $language0
 * @property Category $category
 */
class CategoryTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => CatalogModule::t('common', 'Category'),
            'language' => CatalogModule::t('common', 'Language'),
            'header' => CatalogModule::t('common', 'Header'),
            'title' => CatalogModule::t('common', 'Meta Title'),
            'keywords' => CatalogModule::t('common', 'Meta Keywords'),
            'description' => CatalogModule::t('common', 'Meta Description'),
            'content' => CatalogModule::t('common', 'Content'),
            'enable_mta_title' => Yii::t('common/section', 'Enable MTA for Title'),
            'enable_mta_keywords' => Yii::t('common/section', 'Enable MTA for Keywords'),
            'enable_mta_description' => Yii::t('common/section', 'Enable MTA for Description'),
            'mta_title' => CatalogModule::t('common', 'Material Title'),
            'mta_keywords' => CatalogModule::t('common', 'Material Keywords'),
            'mta_description' => CatalogModule::t('common', 'Material Description'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
