<?php

namespace common\modules\catalog\common\models;

use Yii;
use yii\db\ActiveRecord;
use common\helpers\Upload;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\behaviors\RoutableBehavior;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%sale}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Sale extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sale}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            //SluggableBehavior::className()
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['header', 'description']
            ]
        ];
    }    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['category_id', 'name'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['imageFile'], 'file', 'extensions' => 'gif, jpg, png, jpeg', 'maxSize' => Upload::getMaxUploadSize() * 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('app', 'Категория'),
            'name' => Yii::t('app', 'Название'),
            'image' => Yii::t('common', 'Изображение'),
            'imageFile' => Yii::t('common', 'Изображение'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
            'created_by' => Yii::t('common', 'Создано'),
            'updated_by' => Yii::t('common', 'Обновлено'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(SaleTranslation::className(), ['item_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        // TODO TEST
        return $this->hasMany(Item::className(), ['category_id' => 'id']);
    }
    
    public function getCategory()
    {
        return $this->hasOne(Item::className(), ['id' => 'category_id']);
    }    
}
