<?php

namespace common\modules\catalog\common\models;

use Yii;
use common\models\Language;
use common\modules\catalog\CatalogModule;

/**
 * This is the model class for table "{{%item_translation}}".
 *
 * @property integer $item_id
 * @property string $language
 * @property string $header
 * @property string $header2
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $image_alt
 * @property string $image_title
 * @property string $features_text
 *
 * @property Language $language0
 * @property Item $item
 */
class ItemTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => CatalogModule::t('common', 'Item'),
            'language' => CatalogModule::t('common', 'Language'),
            'header' => CatalogModule::t('common', 'Header'),
            'header2' => CatalogModule::t('common', 'Header2'),
            'title' => CatalogModule::t('common', 'Meta Title'),
            'keywords' => CatalogModule::t('common', 'Meta Keywords'),
            'description' => CatalogModule::t('common', 'Meta Description'),
            'content' => CatalogModule::t('common', 'Content'),
            'features_text' => CatalogModule::t('common', 'Features'),
            'image_alt' => CatalogModule::t('common', 'Image alt'),
            'image_title' => CatalogModule::t('common', 'Image title'),
            'short_content' => CatalogModule::t('common', 'Short Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
