<?php

namespace common\modules\catalog\common\models\query;

use common\modules\catalog\common\models\Category;
use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[\common\modules\catalog\common\models\models\Category]].
 *
 * @see \common\modules\catalog\models\models\Category
 */
class CategoryQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['status' => Category::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status' => Category::STATUS_INACTIVE]);
        return $this;
    }
}
