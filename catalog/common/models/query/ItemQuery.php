<?php

namespace common\modules\catalog\common\models\query;

use common\modules\catalog\common\models\Item;

/**
 * This is the ActiveQuery class for [[\common\modules\catalog\models\models\Item]].
 *
 * @see \common\modules\catalog\models\models\Item
 */
class ItemQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['status' => Item::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status' => Item::STATUS_INACTIVE]);
        return $this;
    }
}
