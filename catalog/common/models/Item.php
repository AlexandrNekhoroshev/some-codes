<?php

namespace common\modules\catalog\common\models;

use Yii;
use yii\helpers\Url;
use common\helpers\Upload;
use common\models\Language;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\behaviors\RoutableBehavior;
use creocoder\translateable\TranslateableBehavior;
use common\models\GalleryItem;
use common\modules\catalog\CatalogModule;

/**
 * This is the model class for table "{{%item}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $slug
 * @property integer $price
 * @property integer $status
 * @property integer $quantity
 * @property integer $is_recommended
 * @property integer $is_new
 * @property integer $is_bestseller
 * @property integer $actions_ids
 * @property integer $pos
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property string $url
 * @property string $link
 * @property string $breadcrumbs
 *
 * ItemTranslation model properties
 * @property string $header
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $image_alt
 * @property string $image_title
 *
 * @property ItemTranslation[] $itemTranslations
 * @property Language[] $languages
 * @property Category $category
 */
class Item extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $imageFile;
    public $actions_ids;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            //SluggableBehavior::className()
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['header', 'header2', 'title', 'description', 'content', 'short_content', 'features_text', 'image_alt', 'image_title']
            ],
            [
                'class' => RoutableBehavior::className(),
                'defaultRoute' => 'catalog/item/view'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'gallery_id', 'status', 'pos', 'created_at', 'updated_at', 'created_by', 'updated_by', 'price', 'quantity'], 'integer'],
            [['is_recommended', 'is_new', 'is_bestseller'], 'boolean'],
            [['slug'], 'required'],
            [['slug', 'image'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
            [['imageFile'], 'file', 'extensions' => 'gif, jpg, png, jpeg', 'maxSize' => Upload::getMaxUploadSize() * 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => CatalogModule::t('common', 'Category'),
            'slug' => CatalogModule::t('common', 'Slug'),
            'image' => CatalogModule::t('common', 'Image'),
            'imageFile' => CatalogModule::t('common', 'Image'),
            'status' => CatalogModule::t('common', 'Status'),
            'gallery_id' => CatalogModule::t('common', 'Gallery Id'),
            'quantity' => CatalogModule::t('common', 'Quantity'),
            'pos' => CatalogModule::t('common', 'Pos'),
            'actions_ids' => CatalogModule::t('common', 'Actions Ids'),
            'is_recommended' => CatalogModule::t('common', 'Is Recommended'),
            'is_new' => CatalogModule::t('common', 'Is New'),
            'is_bestseller' => CatalogModule::t('common', 'Is Bestseller'),
            'price' => CatalogModule::t('common', 'Price'),
            'created_at' => CatalogModule::t('common', 'Created At'),
            'updated_at' => CatalogModule::t('common', 'Updated At'),
            'created_by' => CatalogModule::t('common', 'Created By'),
            'updated_by' => CatalogModule::t('common', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\modules\catalog\common\models\query\ItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\ItemQuery(get_called_class());
    }

    /**
     * Finds one active model
     * @param $id
     * @return array|null|Item
     */
    public static function findActive($id)
    {
        return static::find()->where(['id' => $id])->one();
    }

    /**
     * Find all active models
     * @return array|Item[]
     */
    public static function findAllActive()
    {
        return static::find()->active()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ItemTranslation::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(Language::className(), ['code' => 'language'])->viaTable('{{%item_translation}}', ['item_id' => 'id']);
    }

    public function getUrl()
    {
        return ($this->category !== null ? '/' . $this->category->slug : '') . '/' . $this->slug;
    }

    public function getLink()
    {
        return Url::to([$this->url]);
    }

    public function getBreadcrumbs()
    {
        $breadcrumbs = [];
        if($this->category !== null) {
            $breadcrumbs[] = [
                'label' => $this->category->header,
                'url' => $this->category->link,
            ];
        }
        $breadcrumbs[] = ['label' => $this->header];

        return $breadcrumbs;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasMany(GalleryItem::className(), ['gallery_id' => 'gallery_id'])
            ->where(['status' => GalleryItem::STATUS_ACTIVE])
            ->orderBy('position ASC');
    }
    
}
