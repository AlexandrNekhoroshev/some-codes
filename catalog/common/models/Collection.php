<?php

namespace common\modules\catalog\common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%brand}}".
 *
 * @property integer $id
 * @property integer $manufacturer_id
 * @property string $slug
 * @property string $name
 * @property integer $position
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Collection extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%collection}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'name', 'slug'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manufacturer_id' => Yii::t('app', 'Бренд'),
            'name' => Yii::t('app', 'Название'),
            'slug' => Yii::t('app', 'ЧПУ'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        // TODO TEST
        return $this->hasMany(Item::className(), ['manufacturer_id' => 'id'])
            ->viaTable(Manufacturer::tableName(), ['id' => 'manufacturer_id']);
    }
}
