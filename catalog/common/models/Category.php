<?php

namespace common\modules\catalog\common\models;

use Yii;
use yii\helpers\Url;
use common\models\Language;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\behaviors\RoutableBehavior;
use creocoder\translateable\TranslateableBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use common\modules\catalog\CatalogModule;
use common\helpers\Upload;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property string $slug
 * @property string $price
 * @property integer $items_per_page
 *
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property string $url
 * @property string $link
 * @property string $breadcrumbs
 *
 * CategoryTranslation model properties
 * @property string $header
 * @property string $header2
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $enable_mta_title
 * @property string $enable_mta_keywords
 * @property string $enable_mta_description
 * @property string $mta_title
 * @property string $mta_keywords
 * @property string $mta_description
 *
 * @property CategoryTranslation[] $categoryTranslations
 * @property Language[] $languages
 */
class Category extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $imageFile;

    /**
     * MTA - Meta tags on the algorithm
     */
    const NOT_USE_MTA = 0;
    const USE_MTA = 1;
    //const USE_DEEP_MTA = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            //SluggableBehavior::className()
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => [
                    'header', 'header2', 'title', 'description', 'content',
                    'enable_mta_title', 'enable_mta_keywords', 'enable_mta_description',
                    'mta_title', 'mta_keywords', 'mta_description'
                ]
            ],
            [
                'class' => RoutableBehavior::className(),
                'defaultRoute' => 'catalog/category/view'
            ],
//            'tree' => [
//                'class' => NestedSetsBehavior::className(),
//            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug'], 'required'],
            [['status', 'parent_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'items_per_page'], 'integer'],
            [['slug', 'image'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            ['items_per_page', 'default', 'value' => 10],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['enable_mta_title', 'enable_mta_keywords', 'enable_mta_description'], 'boolean'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
            [['imageFile'], 'file', 'extensions' => 'gif, jpg, png, jpeg', 'maxSize' => Upload::getMaxUploadSize() * 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slug' => CatalogModule::t('common', 'Slug'),
            'use_mta' => CatalogModule::t('common', 'Use meta tags algorithms'),
            'items_per_page' => CatalogModule::t('common', 'Items per page'),
            'status' => CatalogModule::t('common', 'Status'),
            'image' => CatalogModule::t('common', 'Image'),
            'price' => CatalogModule::t('common', 'Price'),
            'imageFile' => CatalogModule::t('common', 'Image'),
            'created_at' => CatalogModule::t('common', 'Created At'),
            'updated_at' => CatalogModule::t('common', 'Updated At'),
            'created_by' => CatalogModule::t('common', 'Created By'),
            'updated_by' => CatalogModule::t('common', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['category_id' => 'id'])->orderBy('pos asc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CategoryTranslation::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(Language::className(), ['code' => 'language'])->viaTable('{{%category_translation}}', ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\catalog\common\models\query\CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\CategoryQuery(get_called_class());
    }

    /**
     * Finds one active model
     * @param $id
     * @return array|null|Category
     */
    public static function findActive($id)
    {
        return static::find()->where(['id' => $id])->active()->one();
    }

    /**
     * Find all active models
     * @return array|Category[]
     */
    public static function findAllActive()
    {
        return static::find()->active()->all();
    }

    public function getUrl()
    {
        return '/' . $this->slug;
    }

    public function getLink()
    {
        return Url::to([$this->url]);
    }

    public function getBreadcrumbs()
    {
        return [
            ['label' => $this->header]
        ];
    }
}
