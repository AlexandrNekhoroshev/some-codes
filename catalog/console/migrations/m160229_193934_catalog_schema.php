<?php

use yii\db\Migration;
use common\modules\catalog\common\models\Category;
use common\modules\catalog\common\models\CategoryTranslation;
use common\modules\catalog\common\models\Item;
use common\modules\catalog\common\models\ItemTranslation;

class m160229_193934_catalog_schema extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Category::tableName(), [
            'id' => $this->primaryKey(),
            'parent_id' =>  $this->integer(4)->notNull(),
            'slug' => $this->string(255)->notNull()->unique(),
            'items_per_page' => $this->smallInteger()->notNull()->defaultValue(10),
            'image' => $this->string(255),
            'status' => $this->smallInteger()->notNull()->defaultValue(Category::STATUS_ACTIVE),
            'tree' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createTable(CategoryTranslation::tableName(), [
            'category_id' => $this->integer(11)->notNull(),
            'language' => $this->string(8)->notNull(),
            'header' => $this->text(),
            'title' => $this->string(255),
            'description' => $this->string(255),
            'content' => $this->text(),
            'enable_mta_title' => $this->smallInteger()->notNull()->defaultValue(Category::NOT_USE_MTA),
            'enable_mta_keywords' => $this->smallInteger()->notNull()->defaultValue(Category::NOT_USE_MTA),
            'enable_mta_description' => $this->smallInteger()->notNull()->defaultValue(Category::NOT_USE_MTA),
            'mta_title' => $this->text(),
            'mta_keywords' => $this->text(),
            'mta_description' => $this->text(),
        ], $tableOptions);

        $this->addPrimaryKey('category_translation_pk', '{{%category_translation}}', ['category_id', 'language']);
//        $this->addForeignKey('fk_category_translation', '{{%category_translation}}', 'category_id', '{{%category}}', 'id', 'cascade', 'cascade');
//        $this->addForeignKey('fk_category_translation_language', '{{%category_translation}}', 'language', '{{%language}}', 'code', 'cascade', 'cascade');

        $this->createTable(Item::tableName(), [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'gallery_id' => $this->integer(4)->notNull()->defaultValue(0),
            'slug' => $this->string(255)->notNull()->unique(),
            'price' => $this->integer(),
            'image' => $this->string(255),
            'status' => $this->smallInteger()->notNull()->defaultValue(Item::STATUS_ACTIVE),
            'quantity' => $this->integer(),
            'is_recommended' => $this->smallInteger()->notNull()->defaultValue(0),
            'is_new' => $this->smallInteger()->notNull()->defaultValue(0),
            'is_bestseller' => $this->smallInteger()->notNull()->defaultValue(0),
            'pos' => $this->integer(4)->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createTable(ItemTranslation::tableName(), [
            'item_id' => $this->integer(11)->notNull(),
            'language' => $this->string(8)->notNull(),
            'header' => $this->text(),
            'title' => $this->string(255),
            'description' => $this->string(255),
            'content' => $this->text(),
            'short_content' => $this->text(),
            'features_text' => $this->text(),
            'image_alt' => $this->string(255),
            'image_title' => $this->string(255),
        ], $tableOptions);

//        $this->addForeignKey('fk_item_category', Item::tableName(), 'category_id', Category::tableName(), 'id', 'cascade', 'cascade');
        $this->addPrimaryKey('item_translation_pk', ItemTranslation::tableName(), ['item_id', 'language']);
//        $this->addForeignKey('fk_item_translation', ItemTranslation::tableName(), 'item_id', Item::tableName(), 'id', 'cascade', 'cascade');
//        $this->addForeignKey('fk_item_translation_language', ItemTranslation::tableName(), 'language', '{{%language}}', 'code', 'cascade', 'cascade');


        $this->createTable('{{%manufacturer}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(),
            'name' => $this->string(),
            'logo' => $this->string(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%collection}}', [
            'id' => $this->primaryKey(),
            'manufacturer_id' => $this->integer(),
            'slug' => $this->string(),
            'name' => $this->string(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
//        $this->dropForeignKey('fk_item_translation_language', ItemTranslation::tableName());
//        $this->dropForeignKey('fk_item_translation', ItemTranslation::tableName());
        $this->dropTable(ItemTranslation::tableName());
//        $this->dropForeignKey('fk_item_category', Item::tableName());
        $this->dropTable(Item::tableName());

//        $this->dropForeignKey('fk_category_translation_language', CategoryTranslation::tableName());
//        $this->dropForeignKey('fk_category_translation', CategoryTranslation::tableName());
        $this->dropTable(CategoryTranslation::tableName());
        $this->dropTable(Category::tableName());

        $this->dropTable('{{%manufacturer}}');
        $this->dropTable('{{%collection}}');
    }
}
