<?php

use yii\db\Migration;
use common\models\Module;

class m160229_193912_catalog_init extends Migration
{
    public function up()
    {
        $this->insert(Module::tableName(), [
            'id' => 'catalog',
            'class' => 'common\modules\catalog\CatalogModule',
            'title' => 'Каталог',
            'icon' => 'ion-clipboard',
            'version' => '0.1',
            'is_installed' => Module::STATE_INSTALLED
        ]);
    }

    public function down()
    {
        $this->delete(Module::tableName(), ['id' => 'catalog']);
    }
}
