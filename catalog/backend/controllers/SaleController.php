<?php

namespace common\modules\catalog\backend\controllers;

use Yii;
use common\helpers\Image;
use common\modules\catalog\common\models\Sale;
use common\modules\catalog\common\models\Item;
use common\modules\booking\common\models\Booking;
use common\modules\catalog\backend\models\SaleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\actions\DeleteImageAction;

setlocale(LC_ALL, 'ru_RU.UTF-8');

/**
 * SaleController implements the CRUD actions for Sale model.
 */
class SaleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => ['calendar','json'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],                
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'delete-image' => [
                'class' => DeleteImageAction::className(),
                'modelClass' => Sale::className(),
                'attribute' => 'image',
            ]
        ];
    }    

    /**
     * Lists all Sale models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Sale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sale();

        if ($model->load(Yii::$app->request->post())) {
            
            foreach (Yii::$app->request->post('SaleTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
            }            
            
            /** image saving */
            if($image = UploadedFile::getInstance($model, 'imageFile')) {
                $model->image = Image::upload($image);
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success',
                    Yii::t('backend', 'Коллекция "{name}" успешно добавлена', ['name' => $model->name]));

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'items' => Item::find()->where(['status' => Item::STATUS_ACTIVE])->all()
        ]);
    }

    /**
     * Updates an existing Sale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            
            foreach (Yii::$app->request->post('SaleTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
            }            
            
            /** image saving */
            if($image = UploadedFile::getInstance($model, 'imageFile')) {
                $model->image = Image::upload($image);
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success',
                    Yii::t('backend', 'Коллекция "{name}" успешно изменена', ['name' => $model->name]));

                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'items' => Item::find()->where(['status' => Item::STATUS_ACTIVE])->all()
        ]);
    }

    /**
     * Deletes an existing Sale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->delete()) {
            Yii::$app->session->setFlash('success',
                Yii::t('backend', 'Sale "{name}" успешно удалена', ['name' => $model->name]));
        }

        return $this->redirect(['index']);
    }
    
    public function actionJson($room_id=NULL)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $days = Booking::find()->where(['status' => 2, 'room_id' => $room_id])->all();
        
        $date_list = array();

        foreach ($days as $day) {

            $date = $day->check_in;
            $date_end = date('Y-m-d H:i:s', strtotime($day->check_out) - 86400);
            while($date <= $date_end){
                $date_list[] = array(
                    'date' => $date, 
                    'title' => $day->rooms
                );
                $date = date('Y-m-d H:i:s', strtotime($date) + 86400);
            }

        }

        return $date_list;
    }   

    public function actionCalendar($date = null)
    {
        
        $items = Sale::find()->where(['status' => Sale::STATUS_ACTIVE])->all();
        
        if(!is_null($date)){
            $current_month = $date;
        } else {
            $current_month = date('Y-m');            
        }
        
        $next_month = date('Y-m',strtotime($current_month.'-01 +1 month'));
        $prev_month = date('Y-m',strtotime($current_month.'-01 -1 month'));
        
        return $this->render('calendar', compact('items','current_month','next_month','prev_month'));
    }    

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'Страница не найдена'));
    }
}
