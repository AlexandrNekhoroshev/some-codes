<?php

namespace common\modules\catalog\backend\controllers;


use Yii;
use common\helpers\Image;
use common\modules\catalog\common\models\Item;
use common\modules\catalog\CatalogModule;
use common\actions\DeleteImageAction;
use common\modules\catalog\backend\models\ItemSearch;
use backend\actions\SwitchStatusAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'switch-status' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'switch-status' => [
                'class' => SwitchStatusAction::className(),
                'modelClass' => Item::className(),
                'statusOn'=> Item::STATUS_ACTIVE,
                'statusOff' => Item::STATUS_INACTIVE,
            ],
            'delete-image' => [
                'class' => DeleteImageAction::className(),
                'modelClass' => Item::className(),
                'attribute' => 'image',
            ]
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            foreach (Yii::$app->request->post('ItemTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
            }
            
            if(Yii::$app->request->post('Item')){
                $action = Yii::$app->request->post('Item'); 
                if(!empty($action['actions_ids']) and count($action['actions_ids']) > 0){
                    $actions_ids = implode(',',array_unique($action['actions_ids']));
                    $model->actions_ids = $actions_ids;                    
                }
            }

            /** image saving */
            if($image = UploadedFile::getInstance($model, 'imageFile')) {
                $model->image = Image::upload($image);
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success', CatalogModule::t('backend', 'Item "{item}" successfully created', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]));
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            foreach (Yii::$app->request->post('ItemTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
            }
            
            if(Yii::$app->request->post('Item')){
                $action = Yii::$app->request->post('Item'); 
                if(!empty($action['actions_ids']) and count($action['actions_ids']) > 0){
                    $actions_ids = implode(',',array_unique($action['actions_ids']));
                    $model->actions_ids = $actions_ids;                    
                }
            }

            /** image saving */
            if($image = UploadedFile::getInstance($model, 'imageFile')) {
                $model->image = Image::upload($image);
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success', CatalogModule::t('backend', 'Item "{item}" successfully updated', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]));
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->delete()) {
            Yii::$app->session->setFlash('success', CatalogModule::t('backend', 'Item "{item}" successfully deleted', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findActive($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }
}
