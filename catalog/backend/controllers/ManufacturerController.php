<?php

namespace common\modules\catalog\backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\modules\catalog\common\models\Manufacturer;
use common\modules\catalog\backend\models\ManufacturerSearch;
use common\modules\catalog\backend\models\LogoUploadForm;
use yii\web\UploadedFile;

/**
 * ManufacturerController implements the CRUD actions for Manufacturer model.
 */
class ManufacturerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Manufacturer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ManufacturerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Manufacturer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Manufacturer();
        $logoUploadForm = new LogoUploadForm();

        if (
            $model->load(Yii::$app->request->post()) &&
            $logoUploadForm->load(Yii::$app->request->post()) &&
            $model->validate()
        ) {

            $logoUploadForm->file = UploadedFile::getInstance($logoUploadForm, 'file');

            if($logoUploadForm->validate()) {

                if($logoUploadForm->file !== null) {
                    $model->logo = $logoUploadForm->file->name;
                }

                if($model->save()) {
                    Yii::$app->session->setFlash('success',
                        Yii::t('backend', 'Бренд "{name}" успешно добавлен', ['name' => $model->name]));
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'logoUploadForm' => $logoUploadForm
        ]);
    }

    /**
     * Updates an existing Manufacturer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $logoUploadForm = new LogoUploadForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success',
                Yii::t('backend', 'Бренд "{name}" успешно добавлен', ['name' => $model->name]));

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'logoUploadForm' => $logoUploadForm
        ]);
    }

    /**
     * Deletes an existing Manufacturer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->delete()) {
            Yii::$app->session->setFlash('success',
                Yii::t('backend', 'Бренд "{name}" успешно удален', ['name' => $model->name]));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Manufacturer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Manufacturer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Manufacturer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'Страница не найдена'));
    }
}
