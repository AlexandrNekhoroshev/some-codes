<?php

namespace common\modules\catalog\backend\controllers;

use Yii;
use common\modules\catalog\common\models\Category;
use common\modules\catalog\backend\models\CategorySearch;
use common\modules\catalog\CatalogModule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\actions\SwitchStatusAction;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\helpers\Image;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'switch-status' => [
                'class' => SwitchStatusAction::className(),
                'modelClass' => Category::className(),
                'statusOn'=> Category::STATUS_ACTIVE,
                'statusOff' => Category::STATUS_INACTIVE,
            ]

        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            foreach (Yii::$app->request->post('CategoryTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
            }

            /** image saving */
            if($image = UploadedFile::getInstance($model, 'imageFile')) {
                $model->image = Image::upload($image);
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success', CatalogModule::t('backend', 'Category "{item}" successfully created', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]));
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            foreach (Yii::$app->request->post('CategoryTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
            }

            /** image saving */
            if($image = UploadedFile::getInstance($model, 'imageFile')) {
                $model->image = Image::upload($image);
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success', CatalogModule::t('backend', 'Category "{item}" successfully updated', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]));
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->delete()) {
            Yii::$app->session->setFlash('success', CatalogModule::t('backend', 'Category "{item}" successfully deleted', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }
}
