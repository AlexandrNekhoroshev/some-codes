<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="brand-form">

    <?php $form = ActiveForm::begin(['id' => 'form', 'layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($logoUploadForm, 'file')->fileInput() ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'status')->dropDownList([
        $model::STATUS_ACTIVE => Yii::t('backend', 'Опубликован'),
        $model::STATUS_NOT_ACTIVE => Yii::t('backend', 'Неопубликован'),
    ])->label('Статус') ?>

    <div class="well">
        <div class="pull-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
