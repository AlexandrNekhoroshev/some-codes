<?php


$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Бренды'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?= $this->render('_form', [
                'model' => $model,
                'logoUploadForm' => $logoUploadForm
            ]) ?>
        </div>
    </div>
</div>