<?php



$this->title = \common\modules\catalog\CatalogModule::t('backend', 'New manufacturer');
$this->params['breadcrumbs'][] = ['label' => \common\modules\catalog\CatalogModule::t('backend', 'Manufacturers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Добавить');

?>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?= $this->render('_form', [
                'model' => $model,
                'logoUploadForm' => $logoUploadForm
            ]) ?>
        </div>
    </div>
</div>