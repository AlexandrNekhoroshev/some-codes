<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = \common\modules\catalog\CatalogModule::t('backend', 'Manufacturers');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="pull-right">
    <?= Html::a('<i class="ion-plus-round"></i> ' . \common\modules\catalog\CatalogModule::t('backend', 'Create manufacturer'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table'],
                'layout' => "{items}\n{pager}\n{summary}",
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'logo',
                        'format' => 'raw',
                        'value' => function($model) {
                            return Html::img('http://logok.org/wp-content/uploads/2014/03/Adidas-Logo.png', ['class' => 'thumbnail', 'width' => 100]);
                        },
                        'enableSorting' => false,
                        'filter' => false
                    ],
                    'name',
                    'slug',
//                    'position',
                    // 'status',
                    // 'created_by',
                    // 'updated_by',
                    // 'created_at',
                    // 'updated_at',

                    ['class' => 'backend\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>