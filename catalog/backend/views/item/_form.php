<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\catalog\common\models\Category;
use common\models\Gallery;
use common\models\Language;
use common\helpers\Image;
use common\models\Material;

/* @var $this yii\web\View */
/* @var $model common\modules\catalog\common\models\Item */
/* @var $form kartik\form\ActiveForm */

$languages = Language::findAllActive();
$lang = Yii::$app->config->get('materialsLanguage');

$categories = [];
$list = Category::find()->active()->all();
array_walk($list, function($model) use (&$categories) {
    $categories[$model->id] = $model->translate(Yii::$app->config->get('materialsLanguage'))->header;
});

$gallery = [];
$listg = Gallery::find()->active()->all();
array_walk($listg, function($model) use (&$gallery) {
    $gallery[$model->id] = $model->translate(Yii::$app->config->get('materialsLanguage'))->header;
});

$actions = [];
$list = Material::find()->where(['section_id' => 3])->active()->all();
array_walk($list, function ($model) use (&$actions) {
    $actions[$model->id] = $model->translate(Yii::$app->config->get('materialsLanguage'))->header;
});

?>

<?php $form = ActiveForm::begin(['id' => 'form', 'type' => ActiveForm::TYPE_HORIZONTAL, 'options' => ['enctype' => 'multipart/form-data']]); ?>

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#tab-general" aria-controls="tab-general" role="tab" data-toggle="tab"><?= Yii::t('backend', 'General') ?></a>
    </li>
    <li role="presentation">
        <a href="#tab-system" aria-controls="tab-system" role="tab" data-toggle="tab"><?= Yii::t('backend', 'System') ?></a>
    </li>
</ul>

<div class="tab-content">

    <div class="tab-pane active" role="tabpanel" id="tab-general">

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'category_id')->dropDownList(
                $categories,
                ['prompt' => '...']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'gallery_id')->dropDownList(
                $gallery,
                ['prompt' => '...']
            ) ?>   
        </div> 
    </div>    

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'slug', [
                'addon' => [
                    'prepend' => [
                        'content' => '/'
                    ]
                ]
            ])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList([
                $model::STATUS_ACTIVE => Yii::t('backend', 'Active'),
                $model::STATUS_INACTIVE => Yii::t('backend', 'Inactive')
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'price')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'quantity')->textInput() ?></div>
    </div>

    <?= $form->field($model, 'imageFile')->fileInput(['data-change' => 'preview', 'accept' => 'image/*']) ?>

    <div class="row">
        <div class="col-md-6">
            <div class="thumbnail preview" data-show="preview">
                <?php if($model->image): ?>
                    <?= Image::thumb($model->image, [], 200, 200) ?>
                    <?= Html::a('<i class="ion-close-round"></i>', ['delete-image', 'id' => $model->id], ['class' => 'btn btn-circle btn-danger']) ?>
                <?php else: ?>
                    <img src="">
                <?php endif ?>
            </div>
        </div>
        <div class="col-md-6"><?= $form->field($model, 'pos')->textInput() ?></div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?php $model->actions_ids = explode(',',$model->actions_ids); ?>
            <?= $form->field($model, 'actions_ids')->listBox(
                $actions,
                [
                    'prompt' => '...',
                    'multiple' =>true
                ]
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'is_recommended')->checkbox() ?>
            <?= $form->field($model, 'is_new')->checkbox() ?>
            <?= $form->field($model, 'is_bestseller')->checkbox() ?>        
        </div>    
    </div>    

    <hr>

    <!-- translations -->

    <ul class="nav nav-tabs" role="tablist">
        <?php $i = 0; foreach ($languages as $language): ?>
            <li role="presentation" class="<?= $language->code == $lang ? 'active' : '' ?>">
                <a href="#lang-<?= $language->code?>" aria-controls="lang-<?= $language->code?>" role="tab" data-toggle="tab">
                    <?=Html::tag('i', '', ['class' => $language->code, 'style' => 'margin-right: 5px;'])?>
                    <?=Yii::t('common/language', $language->title) ?>
                </a>
            </li>
        <?php $i++; endforeach ?>
    </ul>

    <div class="tab-content">

        <?php $j = 0; foreach ($languages as $language): ?>

            <div class="tab-pane <?= $language->code == $lang ? 'active' : '' ?>" role="tabpanel"  id="lang-<?= $language->code?>">

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']header')->textInput() ?>
                
                <?= $form->field($model->translate($language->code), '[' . $language->code . ']header2')->textInput() ?>

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']title')->textInput() ?>

                <?/*= $form->field($model->translate($language->code), '[' . $language->code . ']keywords')->textarea() */?>

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']description')->textarea() ?>

                <hr>
                <?php /* ?>
                <?= $form->field($model->translate($language->code), '[' . $language->code . ']features_text')->widget(\mihaildev\ckeditor\CKEditor::className(), [
                    'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'small',
                        'language' => Yii::$app->language
                    ]),
                ]) ?> <?php */ ?>
                <?= $form->field($model->translate($language->code), '[' . $language->code . ']features_text')->textarea(['rows' => 10]) ?>   

                
                <hr>                

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']short_content')->widget(\mihaildev\ckeditor\CKEditor::className(), [
                    'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'small',
                        'language' => Yii::$app->language
                    ]),
                ]) ?>                
                
                <hr>

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']content')->widget(\mihaildev\ckeditor\CKEditor::className(), [
                    'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',
                        'language' => Yii::$app->language
                    ]),
                ]) ?>

            </div>

        <?php $j++; endforeach ?>

    </div>

    <!-- /translations -->
    </div>
    <div class="tab-pane" role="tabpanel" id="tab-system">
        <?= $form->field($model->route, 'route')->dropDownList(
            [$model->defaultRoute => Yii::t('backend', 'Default')] +
            Yii::$app->params['materialActions']
        ) ?>
    </div>
</div>

<div class="well">
    <div class="pull-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
</div>

<?php ActiveForm::end(); ?>