<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Material;
use common\modules\catalog\common\models\Category;

/* @var $this yii\web\View */
/* @var $section common\models\Section */
/* @var $searchModel backend\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

isset($searchModel->category_id) && ($section = Section::findOne($searchModel->category_id));

$this->title = Yii::t('backend/material', 'Объекты') . (isset($section) ? ' (' . $section->header . ')' : '');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-right">
    <?= Html::a('<i class="ion-plus-round"></i> ' . Yii::t('backend/material', 'Создать объект'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
</div>

<?php Pjax::begin(); ?>
<?php //echo $this->render('_search', ['model' => $searchModel]); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => ['class' => 'table'],
    'layout' => "{items}\n{pager}\n{summary}",
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn'],

        [
            'attribute' => 'image',
            'format' => 'raw',
            'value' => function ($model) {
                return \common\helpers\Image::thumb($model->image, ['class' => 'thumbnail'], 100, 100);
            },
            'options' => ['width' => '8%']
        ],
        [
            'attribute' => 'header',
            'label' => Yii::t('backend/material', 'Header'),
            'value' => function ($model) {
                return $model->translate(Yii::$app->config->get('materialsLanguage'))->header;
            }

        ],
            [
                'attribute' => 'slug',
                'value' => 'url'
            ],
        [
            'attribute' => 'category_id',
            'value' => 'category.header',
            'filter' => Html::activeDropDownList($searchModel, 'category_id',
                ArrayHelper::map(Category::find()->active()->all(), 'id', 'header'),
                ['class'=>'form-control', 'prompt' => '']),
        ],
        // 'created_at',
        'updated_at:datetime',
        // 'created_by',
        // 'updated_by',
        [
            'class' => 'backend\grid\SwitchStatusColumn',
            'checked' => function($model) {
                return $model->status;
            }
        ],
        ['class' => 'backend\grid\ActionColumn'],
    ],
]); ?>
<?php Pjax::end(); ?>