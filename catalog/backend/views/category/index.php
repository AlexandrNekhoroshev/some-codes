<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\modules\catalog\CatalogModule;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = CatalogModule::t('backend', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-right">
    <?= Html::a('<i class="ion-plus-round"></i> ' . CatalogModule::t('backend', 'Create category'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
</div>

<div class="content-wrapper">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table'],
        'layout' => "{items}\n{pager}\n{summary}",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['width' => '8%']
            ],
            [
                'attribute' => 'header',
                'label' => Yii::t('backend/section', 'Header'),
                'value' => function ($model) {
                    return $model->translate(Yii::$app->config->get('materialsLanguage'))->header;
                }

            ],
            'slug',
            //'created_at',
             'updated_at:datetime',
            // 'created_by',
            // 'updated_by',
            [
                'class' => 'backend\grid\SwitchStatusColumn',
                'checked' => function($model) {
                    return $model->status;
                }
            ],
            ['class' => 'backend\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
