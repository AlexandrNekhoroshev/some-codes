<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\catalog\models\Category */

$this->title = Yii::t('backend', 'Update "{item}"', ['item' => $model->translate(Yii::$app->config->get('materialsLanguage'))->header]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/section', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-wrapper">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
