<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\modules\catalog\common\models\Item;


$this->title = Yii::t('backend', 'Продаваемые номера');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="pull-right">
    <?= Html::a('<i class="ion-plus-round"></i> ' . Yii::t('backend', 'Добавить номер'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table'],
            'layout' => "{items}\n{pager}\n{summary}",
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return \common\helpers\Image::thumb($model->image, ['class' => 'thumbnail'], 100, 100);
                    },
                    'options' => ['width' => '8%']
                ],                
                'name',
                [
                    'attribute' => 'category_id',
                    'value' => 'category.header',
                    'filter' => Html::activeDropDownList($searchModel, 'category_id',
                        ArrayHelper::map(Item::find()->where(['status' => 1])->all(), 'id', 'header'),
                        ['class'=>'form-control', 'prompt' => '']),                    
                ],
                [
                    'class' => 'backend\grid\SwitchStatusColumn',
                    'checked' => function($model) {
                        return $model->status;
                    }
                ],
                ['class' => 'backend\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
        </div>
    </div>
</div>