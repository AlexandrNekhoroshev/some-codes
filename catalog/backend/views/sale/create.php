<?php

use admin\helpers\Toolbar;

/* @var $this yii\web\View */
/* @var $model common\models\catalog\Collection */

$this->title = Yii::t('backend', 'Новая коллекция');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Продаваемые номера'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Добавить');

?>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?= $this->render('_form', [
                'model' => $model,
                'items' => $items
            ]) ?>
        </div>
    </div>
</div>