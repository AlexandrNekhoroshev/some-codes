<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\modules\catalog\common\models\Item;


$this->title = Yii::t('backend', 'Календарь занятости');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="pull-right">
    <?= Html::a('<i class="ion-plus-round"></i> ' . \common\modules\catalog\CatalogModule::t('backend', 'Create item'), ['/booking/booking/create'], ['class' => 'btn btn-sm btn-primary']) ?>
</div>
<br>
<p align="center">
    <a href="/admin/catalog/sale/calendar?date=<?= $prev_month ?>">
        <i class=" glyphicon glyphicon-chevron-left"></i>
    </a>
    <b><?= strftime("%B %Y", strtotime($current_month)) ?></b>
    <a href="/admin/catalog/sale/calendar?date=<?= $next_month ?>">
        <i class=" glyphicon glyphicon-chevron-right"></i>
    </a>
</p>

<div class="clearfix"></div><br/>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?php foreach($items as $item): ?>
                <div class="row">
                    <div class="col-md-3"><h4><?= $item->name ?></h4></div>
                    <div class="col-md-9">
                        <div id="calendar_<?= $item->id ?>"></div>                        
                        <script>
                            setTimeout(function(){
                                $(document).ready(function() {
                                    $('#calendar_<?= $item->id ?>').eventCalendar({
                                        eventsjson: '/admin/catalog/sale/json?room_id=<?= $item->id ?>',
                                        showDayAsWeeks: false,
                                        jsonDateFormat: 'human',
                                        startWeekOnMonday: false,
                                        openEventInNewWindow: true,
                                        dateFormat: 'dddd DD-MM-YYYY',
                                        showDescription: false,                                       
                                    });
                                });
                            }, 300);
                        </script>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>