<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\catalog\models\Sale */

$this->title = Yii::t('backend', 'Update "{item}"', ['item' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/material', 'Продаваемые номера'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?= $this->render('_form', [
                'model' => $model,
                'items' => $items
            ]) ?>
        </div>
    </div>
</div>