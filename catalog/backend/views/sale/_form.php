<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use common\modules\catalog\common\models\Item;
use common\helpers\Image;
use common\models\Language;

$languages = Language::findAllActive();
$lang = Yii::$app->config->get('materialsLanguage');

/* @var $this yii\web\View */
/* @var $model common\models\catalog\Collection */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="collection-form">

    <?php $form = ActiveForm::begin(['id' => 'form', 'type' => ActiveForm::TYPE_HORIZONTAL]); ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map($items, 'id', 'header')
    ) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'status')->dropDownList([
        Item::STATUS_ACTIVE => Yii::t('backend', 'Опубликован'),
        Item::STATUS_INACTIVE => Yii::t('backend', 'Неопубликован'),
    ])->label('Статус') ?>
    
    <?= $form->field($model, 'imageFile')->fileInput(['data-change' => 'preview', 'accept' => 'image/*']) ?>

    <div class="row">
        <div class="col-md-6">
            <div class="thumbnail preview" data-show="preview">
                <?php if($model->image): ?>
                    <?= Image::thumb($model->image, [], 200, 200) ?>
                    <?= Html::a('<i class="ion-close-round"></i>', ['delete-image', 'id' => $model->id], ['class' => 'btn btn-circle btn-danger']) ?>
                <?php else: ?>
                    <img src="">
                <?php endif ?>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>   

    <!-- translations -->

    <ul class="nav nav-tabs" role="tablist">
        <?php $i = 0; foreach ($languages as $language): ?>
            <li role="presentation" class="<?= $language->code == $lang ? 'active' : '' ?>">
                <a href="#lang-<?= $language->code?>" aria-controls="lang-<?= $language->code?>" role="tab" data-toggle="tab">
                    <?=Html::tag('i', '', ['class' => $language->code, 'style' => 'margin-right: 5px;'])?>
                    <?=Yii::t('common/language', $language->title) ?>
                </a>
            </li>
        <?php $i++; endforeach ?>
    </ul>

<div class="tab-content">

        <?php $j = 0; foreach ($languages as $language): ?>

            <div class="tab-pane <?= $language->code == $lang ? 'active' : '' ?>" role="tabpanel"  id="lang-<?= $language->code?>">

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']header')->textInput() ?>
                
                <hr>                

                <?= $form->field($model->translate($language->code), '[' . $language->code . ']description')->widget(\mihaildev\ckeditor\CKEditor::className(), [
                    'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',
                        'language' => Yii::$app->language
                    ]),
                ]) ?>

            </div>

        <?php $j++; endforeach ?>

    </div>

    <!-- /translations -->    

    <div class="well">
        <div class="pull-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div id="eventCalendar" style="width: 300px; margin: 50px auto; padding: 0 0 5px 0;"></div> 


<script>
    setTimeout(function(){
        calendarDays(<?= $model->id ?>)
    }, 1000);
</script>