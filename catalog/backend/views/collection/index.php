<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


$this->title = Yii::t('backend', 'Коллекции');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="pull-right">
    <?= Html::a('<i class="ion-plus-round"></i> ' . Yii::t('backend', 'Создать коллекцию'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'table'],
            'layout' => "{items}\n{pager}\n{summary}",
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                [
                    'attribute' => 'manufacturer_id',
                    'value' => 'manufacturer.name'
                ],
                'slug',
                //'position',
                // 'status',
                // 'created_by',
                // 'updated_by',
                // 'created_at',
                // 'updated_at',

                ['class' => 'backend\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
        </div>
    </div>
</div>