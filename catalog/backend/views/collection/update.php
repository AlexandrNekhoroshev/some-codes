<?php

use admin\helpers\Toolbar;

/* @var $this yii\web\View */
/* @var $model common\models\catalog\Collection */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Коллекции'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['toolbar'][] = Toolbar::link('ion-close-circled red', Yii::t('backend', 'Удалить'), ['delete', 'id' => $model->id], ['data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить?'), 'data-method' => 'post']);
$this->params['toolbar'][] = Toolbar::button('ion-checkmark-circled', Yii::t('backend', 'Сохранить'), ['onclick' => '$("#form").trigger("submit")']);
?>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox">
        <div class="ibox-content">
            <?= $this->render('_form', [
                'model' => $model,
                'brands' => $brands
            ]) ?>
        </div>
    </div>
</div>