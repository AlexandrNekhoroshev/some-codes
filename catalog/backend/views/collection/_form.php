<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\modules\catalog\common\models\Brand;
use common\modules\catalog\common\models\Collection;

/* @var $this yii\web\View */
/* @var $model common\models\catalog\Collection */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="collection-form">

    <?php $form = ActiveForm::begin(['id' => 'form', 'layout' => 'horizontal']); ?>

    <?= $form->field($model, 'manufacturer_id')->dropDownList(
        ArrayHelper::map($brands, 'id', 'name')
    ) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="hr-line-dashed"></div>

    <?= $form->field($model, 'status')->dropDownList([
        Collection::STATUS_ACTIVE => Yii::t('backend', 'Опубликован'),
        Collection::STATUS_NOT_ACTIVE => Yii::t('backend', 'Неопубликован'),
    ])->label('Статус') ?>

    <div class="well">
        <div class="pull-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
