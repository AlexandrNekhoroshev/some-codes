<?php

namespace common\modules\catalog\backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class LogoUploadForm extends Model
{
    /**
     * @var \yii\web\UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'gif, jpg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => Yii::t('backend', 'Лого'),
        ];
    }
}