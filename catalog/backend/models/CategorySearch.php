<?php

namespace common\modules\catalog\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\catalog\common\models\Category;
use common\modules\catalog\common\models\CategoryTranslation;
use backend\behaviors\TranslateableBehavior;

/**
 * CategorySearch represents the model behind the search form about `common\modules\catalog\models\Category`.
 */
class CategorySearch extends Category
{
    public $header;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['slug', 'header'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['translateable']['class'] = TranslateableBehavior::className();

        return $behaviors;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        $query->joinWith('translations');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ],
        ]);

        $dataProvider->sort->attributes['header'] = [

            'asc' => [CategoryTranslation::tableName() . '.header' => SORT_ASC],
            'desc' => [CategoryTranslation::tableName() . '.header' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', CategoryTranslation::tableName() . '.header', $this->header]);

        return $dataProvider;
    }
}
