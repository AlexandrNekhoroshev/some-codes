<?php

namespace common\modules\catalog\frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\modules\catalog\common\models\Item;

class ItemController extends Controller
{
    /**
     * Displays default item page.
     *
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        $item = $this->findModel($id);
        $this->registerMeta($item);
        
        return $this->render('view', [
            'item' => $item,
        ]);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }

    /**
     * Prepare and register Meta tags to \yii\web\View
     * If you still want to make the "Kostil" with meta tags you can do it right here
     * @param $model Item
     */
    protected function registerMeta($model)
    {
        $view = Yii::$app->view;
        $category = $model->category;

        if($category !== null && $category->enable_mta_title) {
            $title = str_replace('%header%', $model->header , $category->mta_title);
        } else {
            $title = $model->title;
        }

        if($category !== null && $category->enable_mta_description) {
            $description = str_replace('%header%', $model->header , $category->mta_description);
        } else {
            $description = $model->description;
        }

        /*if($category !== null && $category->enable_mta_keywords) {
            $keywords = str_replace('%header%', $model->header , $category->mta_keywords);
        } else {
            $keywords = $model->keywords;
        }*/


        $view->title = $title;
        $view->registerMetaTag(['name' => 'description', 'content' => $description]);
        //$view->registerMetaTag(['name' => 'keywords', 'content' => $keywords]);
    }
}
