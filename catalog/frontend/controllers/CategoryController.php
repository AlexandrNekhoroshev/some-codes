<?php

namespace common\modules\catalog\frontend\controllers;

use Yii;
use common\modules\catalog\common\models\Category;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\modules\catalog\common\models\Item;

class CategoryController extends Controller
{
    public function actionView($id)
    {
        $category = $this->findModel($id);
        $this->registerMeta($category);

        $materialsProvider = new ActiveDataProvider([
            'query' => $category->getItems(),
            'pagination' => [
                'pageSize' => $category->items_per_page,
                'defaultPageSize' => $category->items_per_page
            ]
        ]);

        return $this->render('view', [
            'category' => $category,
            'materialsProvider' => $materialsProvider
        ]);
    }


    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findActive($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(t('The requested page does not exist.'));
        }
    }


    protected function registerMeta($model)
    {
        $view = Yii::$app->view;
        $view->title = $model->title;
        $view->registerMetaTag(['name' => 'description', 'content' => $model->description]);
        //$view->registerMetaTag(['name' => 'keywords', 'content' => $model->keywords]);
    }
}
