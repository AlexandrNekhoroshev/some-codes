<?php

namespace common\modules\catalog;

use Yii;
use yii\base\Module;
use yii\web\Application as WebApplication;
use yii\console\Application as ConsoleApplication;
use backend\components\BackendApplication;

class CatalogModule extends Module
{
    //public $enableSlugNesting = true;

    public $controllerNamespace = 'common\modules\catalog\frontend\controllers';

    public function init()
    {
        parent::init();
        if (Yii::$app instanceof BackendApplication) {
            $this->controllerNamespace = 'common\modules\catalog\backend\controllers';
            $this->viewPath = '@common/modules/catalog/backend/views';
            $this->defaultRoute = 'item/index';
        } else if(Yii::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'common\modules\catalog\console\controllers';
        } else if(Yii::$app instanceof WebApplication) {
            $this->viewPath = '@frontend/themes/magichotel/views/catalog';
        }

        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/catalog/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@common/modules/catalog/common/messages',
            'fileMap' => [
                'modules/catalog/backend' => 'backend.php',
                'modules/catalog/frontend' => 'frontend.php',
                'modules/catalog/common' => 'common.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/catalog/' . $category, $message, $params, $language);
    }

    public static function getBackendMenu()
    {
        return [
            ['label' => self::t('backend', 'Categories'), 'url' => ['/catalog/category']],
            ['label' => self::t('backend', 'Items'), 'url' => ['/catalog/item']],
            ['label' => self::t('backend', 'Sale'), 'url' => ['/catalog/sale']],
            ['label' => self::t('backend', 'Calendar'), 'url' => ['/catalog/sale/calendar']],
            ['label' => self::t('backend', 'Manufacturers'), 'url' => ['/catalog/manufacturer']],
            ['label' => self::t('backend', 'Collections'), 'url' => ['/catalog/collection']],
        ];
    }
}