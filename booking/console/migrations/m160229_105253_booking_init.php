<?php

use yii\db\Migration;
use common\models\Module;

class m160229_105253_booking_init extends Migration
{
    public function up()
    {
        $this->insert(Module::tableName(), [
            'id' => 'booking',
            'class' => 'common\modules\booking\BookingModule',
            'title' => 'Бронирование',
            'icon' => 'ion-calendar',
            'version' => '0.1',
            'is_installed' => Module::STATE_INSTALLED
        ]);
    }

    public function down()
    {
        $this->delete(Module::tableName(), ['id' => 'booking']);
    }
}
