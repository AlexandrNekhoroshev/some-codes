<?php

use yii\db\Migration;
use common\modules\booking\common\models\Booking;

class m160229_105322_booking_schema extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Booking::tableName(), [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'email' => $this->string(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'check_in' => $this->string(),
            'check_out' => $this->string(),
            'adults' => $this->integer(),
            'children' => $this->integer(),
            'comment' => $this->text(),
            'rooms' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(Booking::STATUS_NEW),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Booking::tableName());
    }
}
