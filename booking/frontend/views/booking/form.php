<?php
use yii\helpers\Html;
use frontend\themes\magichotel\assets\MagichotelThemeAsset;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\Image;
use yii\jui\DatePicker;
use common\models\Material;
use yii\i18n\Formatter;
use common\modules\booking\common\models\BookingForm;
use common\modules\booking\common\models\Booking;
use common\modules\catalog\common\models\Item;
use common\modules\block\widgets\Block;
use common\widgets\ColumnView;
use common\models\ConfigItem;

$rooms = array('0' => t('Номера'));

foreach(Item::find()->active()->all() as $room){
    $rooms[$room->id] = $room->header;
}

$adults = array();
for($i = 1; $i <= 5; $i++){ $adults[$i] = $i  . ' ' . t('Взрослых'); }

$children = array();
for($i = 0; $i <= 4; $i++){ $children[$i] = $i . ' ' . t('Детей'); }

$bundle = MagichotelThemeAsset::register($this); 
$form = new BookingForm();

if($_POST){
    $form->check_in = $_POST["BookingForm"]["check_in"];
    $form->check_out = $_POST["BookingForm"]["check_out"];
    $form->adults = $_POST["BookingForm"]["adults"];
    $form->children = $_POST["BookingForm"]["children"];    
    $form->item_id = (isset($_POST["BookingForm"]["rooms"])?$_POST["BookingForm"]["rooms"]:'0');    
}

?>

<!-- Header | Start -->
<header>
    <div id="header">
        <div class="h1">
            <h1><span><?= $material->header ?></span>
            <span class="tagline"><?= $material->header2 ?></span></h1>
        </div>
    </div>
</header>
<!-- Header | END -->
<!-- Content | START -->
<main>
    <div class="centre">
    
        <!-- Contact Form | START -->
        <div id="contact" class="b-form-booking">
            <?php if($material->image): ?>
                <img src="<?= Image::getThumbPath($material->image, 1200, 400) ?>" width="1200" height="400" alt="" />
            <?php endif; ?>    

            <?php if(Yii::$app->session->hasFlash('successbooking')){ ?>
                <div>
                    <p class="alert alert-success"><?php echo Yii::$app->session->getFlash('successbooking'); ?></p>
                </div>
            <?php } elseif (Yii::$app->session->hasFlash('errorbooking')) { ?>
                <div>
                    <p class="alert alert-error"><?php echo Yii::$app->session->getFlash('errorbooking'); ?></p>
                </div>                           
            <?php } ?>   

        <?php if(Yii::$app->request->post()): ?>    
            <?php $bookingForm = ActiveForm::begin(['options' => ['class' => ''],]) ?>

                <div class="col">
                    <div class="field mandatory">
                        <?= $bookingForm->field($form, 'name')->textInput(['id' => 'contact-name', 'placeholder' => t('Ваше имя')])->label(false) ?>
                    </div>
                    <div class="field mandatory">
                        <?= $bookingForm->field($form, 'email')->textInput(['id' => 'contact-email', 'placeholder' => 'Email'])->label(false) ?>
                    </div>
                    <div class="field mandatory">
                        <?= $bookingForm->field($form, 'phone')->textInput(['id' => 'contact-phone', 'placeholder' => t('Телефон')])->label(false) ?>
                    </div>
                    <div class="mandatory">
                        <?= $bookingForm->field($form, 'i_agree')->checkbox(['label' => '«Я ознакомлен с <a href="'.Url::to('@web/order-of-booking').'">порядком бронирования</a> и <a href="'.Url::to('@web/accommodation-conditions').'">условиями проживания в отеле</a>, а также согласен с <a href="'.Url::to('@web/processing-of-personal-data').'">обработкой персональных данных</a>»']) ?>
                    </div>
                </div>
                <div class="col">
                    <div class="field calendar">
                        <?php echo DatePicker::widget([
                            'name'  => 'BookingForm[check_in]',
                            'value' => $form->check_in,
                            'clientOptions' => ['minDate' => '0d', 'prevText' => '', 'nextText' =>''],
                            'options' => ['placeholder' => t('Дата заезда'), 'id' => 'contact-arrival', 'readonly' => 'readonly'],
                            'language' => Yii::$app->language,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>
                        <i class="fa fa-calendar-o"></i>
                    </div>
                    <div class="field calendar">
                        <?php echo DatePicker::widget([
                            'name'  => 'BookingForm[check_out]',
                            'value' => $form->check_out,
                            'clientOptions' => ['minDate' => '+1d', 'prevText' => '', 'nextText' =>''],
                            'options' => ['placeholder' => t('Дата выезда'), 'id' => 'contact-departure', 'readonly' => 'readonly'],
                            'language' => Yii::$app->language,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>
                    <i class="fa fa-calendar-o"></i>
                    </div>
                    <div class="select">
                        <?= $bookingForm->field($form, 'item_id')->dropDownList($rooms, ['id' => 'contact-rooms', 'class' => 'infants'], ['options' => [$form->item_id => ['selected' => true]]])->label(false); ?>
                        <?= $bookingForm->field($form, 'adults')->dropdownList( $adults, ['id' => 'contact-adults', 'class' => 'adults'])->label(false); ?>
                        <?= $bookingForm->field($form, 'children')->dropdownList( $children, ['id' => 'contact-children', 'class' => 'children'])->label(false); ?> 
                    </div>
                </div>
                <div class="col">
                    <div class="field">
                        <?= $bookingForm->field($form, 'comment')->textarea(['id' => 'contact-message', 'placeholder' => t('Сообщение')])->label(false) ?>
                    </div>
                    <div class="mandatory b-info-rooms-sum">
                        Стоимость проживания в номере <span id="frm-room-name"></span> <br/>
                        составляет <span id="frm-room-price"></span> рублей за весь период.
                    </div>
                </div>
                <?= $bookingForm->field($form, 'rooms')->hiddenInput()->label(false) ?>
                <?= $bookingForm->field($form, 'room_id')->hiddenInput()->label(false) ?>
                <div style="display: block;width: 100%;clear: both;">
                    <?= Html::submitButton(t('Забронировать'), ['class' => 'btn btn-style-6']) ?>
                </div>
            <?php ActiveForm::end() ?>
        <?php endif; ?>    
        </div>   

        <div class="b-list-rooms">
        <!-- List Items (Rooms) | START -->
        <?php if(ConfigItem::findByKey('booking_status')->value == 1 or ConfigItem::findByKey('booking_status')->value == 2): ?>
            <?php if(!empty($roomsList)): ?>
                <?php foreach($roomsList as $room): ?>
                    <section id="rooms" class="list">
                        <div class="item">
                            <div class="imgcontainer"><img alt="" src="<?=Image::getThumbPath($room->image, 1200, 400)?>" width="1200" height="400" /></div>
                            <div class="details">
                                <h3 class="title"><?= $room->header ?><br />
                                <span> <?= Booking::getPricePeriod($form->check_in, $form->check_out, $form->item_id) ?> рублей</span></h3>
                                <?= $room->description ?>
                                <div class="button"><span data-hover="<?= t('Забронировать') ?>" data-roomid="<?= $room->id ?>" class="button-booking"><?= t('Забронировать') ?></span></div>
                            </div>
                        </div>
                    </section>                
                <?php endforeach; ?>
            <?php else: ?>
                <?php if(ConfigItem::findByKey('booking_status')->value == 1): ?>
                    <p><b><?= Block::widget(['key' => 'text_booking_1']) ?></b></p>
                <?php endif; ?>    
            <?php endif; ?>
        <?php endif; ?>    
            
        <?php if(ConfigItem::findByKey('booking_status')->value == 2): ?>    
            <?php if(!empty($sameRoomsList)): ?>
                <p><b><?= Block::widget(['key' => 'text_booking_4']) ?></b></p>
                <?php foreach($sameRoomsList as $room_s): ?>
                    <section id="rooms" class="list">
                        <div class="item">
                            <div class="imgcontainer"><img alt="" src="<?=Image::getThumbPath($room_s->image, 1200, 400)?>" width="1200" height="400" /></div>
                            <div class="details">
                                <h3 class="title"><?= $room_s->header ?><br />
                                <span> <?= Booking::getPricePeriod($form->check_in, $form->check_out, $form->item_id) ?> рублей</span></h3>
                                <?= $room_s->description ?>
                                <div class="button"><span data-hover="<?= t('Забронировать') ?>" data-roomid="<?= $room_s->id ?>" class="button-booking"><?= t('Забронировать') ?></span></div>
                            </div>
                        </div>
                    </section>                
                <?php endforeach; ?>
            <?php else: ?>
                <p><b><?= Block::widget(['key' => 'text_booking_2']) ?></b></p>
            <?php endif; ?>            
        <?php endif; ?>  

        <?php if(ConfigItem::findByKey('booking_status')->value == 3): ?>    
            <?php if(!empty($allRoomsList)): ?>
                <?php foreach($allRoomsList as $room_a): ?>
                    <section id="rooms" class="list">
                        <div class="item">
                            <div class="imgcontainer"><img alt="" src="<?=Image::getThumbPath($room_a->image, 1200, 400)?>" width="1200" height="400" /></div>
                            <div class="details">
                                <h3 class="title"><?= $room_a->header ?><br />
                                <span> <?= Booking::getPricePeriod($form->check_in, $form->check_out, $form->item_id) ?> рублей</span></h3>
                                <?= $room_a->description ?>
                                <div class="button"><span data-hover="<?= t('Забронировать') ?>" data-roomid="<?= $room_a->id ?>" class="button-booking"><?= t('Забронировать') ?></span></div>
                            </div>
                        </div>
                    </section>                
                <?php endforeach; ?>
            <?php else: ?>
                <p><b><?= Block::widget(['key' => 'text_booking_3']) ?></b></p>
            <?php endif; ?>            
        <?php endif; ?>   

        <?php if(empty($roomsList) or empty($sameRoomsList) or empty($allRoomsList)): ?>  
            <br><br>
            <div class="b-home b-booking"><?= $this->render('../../../../../../frontend/themes/magichotel/views/blocks/booking') ?></div>
            <br>        
        <?php endif; ?>        
            
        <!-- List Items (Rooms) | END -->
        </div>
        
        <!-- Contact Form | END -->
        <?= $material->content ?>
    </div>
    <!-- Google Map | START -->
        <?= Block::widget(['key' => 'block_map']) ?>
    <!-- Google Map | END -->
</main>
<!-- Content | END -->    