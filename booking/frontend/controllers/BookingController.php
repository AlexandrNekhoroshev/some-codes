<?php

namespace common\modules\booking\frontend\controllers;

use Yii;
use frontend\controllers\MaterialController;
use common\modules\booking\common\models\BookingForm;
use common\modules\catalog\common\models\Sale;
use common\modules\booking\common\models\Booking;
use common\models\ConfigItem;

class BookingController extends MaterialController
{
    public function actionIndex($id)
    {
        $material = $this->findMaterialModel($id);
        $this->registerMaterialMeta($material);

        $bookingForm = new BookingForm();
        
        $roomsList = '';
        $sameRoomsList = '';
        $allRoomsList = '';
        
        if(Yii::$app->request->post()){
            $postBookingForm = Yii::$app->request->post('BookingForm');

            if(!empty($postBookingForm['check_in']) and !empty($postBookingForm['check_out'])){
                if(ConfigItem::findByKey('booking_status')->value == 1 or ConfigItem::findByKey('booking_status')->value == 2){
                    $roomsList = Sale::find()
                                    ->where(['=', 'category_id', $postBookingForm['rooms']]) 
                                    ->andWhere(['not in', 'id', Booking::find()->select('room_id')
                                                                    ->where(['<>', 'status', 2])                        
                                                                    ->andWhere(['=', 'item_id', $postBookingForm['rooms']])                        
                                                                    ->andWhere(['not between', 'check_in', $postBookingForm['check_in'], $postBookingForm['check_out']])
                                                                    ->andWhere(['not between', 'check_out', $postBookingForm['check_in'], $postBookingForm['check_out']])
                                                                    ->distinct()
                                                                    ->asArray()])
                                    ->andWhere(['=', 'status', 1]) 
                                    ->all();
                    $sameRoomsList = Sale::find()
                                    ->where(['<>', 'category_id', $postBookingForm['rooms']]) 
                                    ->andWhere(['not in', 'id', Booking::find()->select('room_id')
                                                                    ->where(['<>', 'status', 2])                        
                                                                    ->andWhere(['=', 'item_id', $postBookingForm['rooms']])                        
                                                                    ->andWhere(['not between', 'check_in', $postBookingForm['check_in'], $postBookingForm['check_out']])
                                                                    ->andWhere(['not between', 'check_out', $postBookingForm['check_in'], $postBookingForm['check_out']])
                                                                    ->distinct()
                                                                    ->asArray()])
                                    ->andWhere(['=', 'status', 1]) 
                                    ->all();                     
                }
                
                if(ConfigItem::findByKey('booking_status')->value == 3){
                    $allRoomsList = Sale::find()
                                    ->andWhere(['not in', 'id', Booking::find()->select('room_id')
                                                                    ->where(['<>', 'status', 2])                        
                                                                    ->andWhere(['=', 'item_id', $postBookingForm['rooms']])                        
                                                                    ->andWhere(['not between', 'check_in', $postBookingForm['check_in'], $postBookingForm['check_out']])
                                                                    ->andWhere(['not between', 'check_out', $postBookingForm['check_in'], $postBookingForm['check_out']])
                                                                    ->distinct()
                                                                    ->asArray()])
                                    ->andWhere(['=', 'status', 1]) 
                                    ->all(); 
                }
            }     
        }       

        if ($bookingForm->load(Yii::$app->request->post()) && $bookingForm->saveModel()) {
            $bookingForm->sendEmail();
            return $this->redirect('/contact-ок');
        }

        return $this->render('form', [
            'material' => $material,
            'bookingForm' => $bookingForm,
            'roomsList' => $roomsList,
            'sameRoomsList' => $sameRoomsList,
            'allRoomsList' => $allRoomsList
        ]);
    }
}