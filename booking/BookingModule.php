<?php

namespace common\modules\booking;

use Yii;
use yii\base\Module;
use yii\web\Application as WebApplication;
use yii\console\Application as ConsoleApplication;
use backend\components\BackendApplication;

/**
 * BookingModule
 * Adds a booking functionality for a CatalogModule
 */
class BookingModule extends Module
{
    public $controllerNamespace = 'common\modules\booking\frontend\controllers';

    public function init()
    {
        parent::init();
        if (Yii::$app instanceof BackendApplication) {
            $this->controllerNamespace = 'common\modules\booking\backend\controllers';
            $this->viewPath = '@common/modules/booking/backend/views';
            $this->defaultRoute = 'booking/index';
        } else if(Yii::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'common\modules\booking\console\controllers';
        } else if(Yii::$app instanceof WebApplication) {
            $this->viewPath = '@common/modules/booking/frontend/views';
        }

        $this->registerTranslations();
    }


    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/booking/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@common/modules/booking/common/messages',
            'fileMap' => [
                'modules/booking/backend' => 'backend.php',
                'modules/booking/frontend' => 'frontend.php',
                'modules/booking/common' => 'common.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/booking/' . $category, $message, $params, $language);
    }
}