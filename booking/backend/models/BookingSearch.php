<?php

namespace common\modules\booking\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\booking\common\models\Booking;

/**
 * BookingSearch represents the model behind the search form about `common\modules\booking\models\Booking`.
 */
class BookingSearch extends Booking
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'item_id', 'room_id', 'adults', 'children', 'status', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['check_in', 'check_out', 'created_at'], 'string'],
            [['email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Booking::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'item_id' => $this->item_id,
            'room_id' => $this->room_id,
            'adults' => $this->adults,
            'children' => $this->children,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        
        $query->andFilterWhere(['between', 'check_in', $this->check_in, $this->check_in]);
        $query->andFilterWhere(['between', 'check_out', $this->check_out, $this->check_out]);
        $query->andFilterWhere(['between', 'DATE_FORMAT(FROM_UNIXTIME(created_at), "%Y-%m-%d")', $this->created_at, $this->created_at]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);
            
        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}
