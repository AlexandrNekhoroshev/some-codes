<?php

namespace common\modules\booking\backend\widgets;

use yii\base\Widget;
use common\modules\booking\models\Booking;

class LatestBookings extends Widget
{
    public $limit = 3;

    public function run()
    {
        $latest = Booking::find()->active()->limit($this->limit)->all();

    }
}