<?php

namespace common\modules\booking\backend\controllers;

use common\modules\booking\common\models\BookingForm;
use Yii;
use common\modules\booking\common\models\Booking;
use common\modules\booking\backend\models\BookingSearch;
use common\modules\catalog\common\models\Sale;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BookingController implements the CRUD actions for Booking model.
 */
class BookingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create','salelist','sale-calendar'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Booking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BookingForm();

        if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Booking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new BookingForm();
        $model->setModel($this->findModel($id));
        
        if ($model->load(Yii::$app->request->post()) && $model->saveModel()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSalelist($id){
        $salerooms = Sale::find()->where(['category_id' => $id, 'status' => 1])->all();
        
        $selectlist = '';
        if(count($salerooms) > 0){
            foreach($salerooms as $room){
                $selectlist .= '<option value="' . $room->id . '">' . $room->name . '</option>';
            }
        } else {
            $selectlist .= '<option value="0"></option>';
        }
        
        echo $selectlist;
    }
    
    public function actionSaleCalendar($id){
        $salerooms = Sale::find()->where(['category_id' => $id, 'status' => 1])->all();
        
        $calendar = $this->renderPartial('calendar', [
                'items' => $salerooms,
        ]);
        
        echo $calendar;
    }    

    /**
     * Deletes an existing Booking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
