<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use common\modules\catalog\common\models\Item;
use common\modules\catalog\common\models\Sale;
use common\modules\booking\common\models\Booking;
use common\modules\booking\BookingModule;

if(!empty($model->check_in) and !empty($model->check_out) and empty($model->room_id)){
    $roomsList = Sale::find()
                        ->where(['=', 'category_id', $model->item_id]) 
                        ->andWhere(['not in', 'id', Booking::find()->select('room_id')
                                        ->where(['<>', 'status', 2])                        
                                        ->andWhere(['=', 'item_id', $model->item_id])                        
                                        ->andWhere(['not between', 'check_in', $model->check_in, $model->check_out])
                                        ->andWhere(['not between', 'check_out', $model->check_in, $model->check_out])
                                        ->distinct()
                                        ->asArray()])
                        ->andWhere(['=', 'status', 1]) 
                        ->all();
} else {
    $roomsList = Sale::find()->where(['category_id' => $model->item_id,'status' => 1,])->all();
}

/* @var $this yii\web\View */
/* @var $model common\modules\booking\common\models\BookingForm */
/* @var $form kartik\form\ActiveForm */
?>

<div class="booking-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); ?>

    <?= $form->field($model, 'item_id')->dropDownList(
        ArrayHelper::map(Item::find()->active()->all(), 'id', 'header'),
        [
            'prompt' => 'Выберите категорию...',
            'onchange' => '
                $.post("salelist?id='.'"+$(this).val(), function(data){
                    $("select#bookingform-room_id").html(data);
                });
                $.post("sale-calendar?id='.'"+$(this).val(), function(data){
                    $("#calendar_all").html(data);
                });
            '
        ]) ?>
    
    <?= $form->field($model, 'room_id')->dropDownList(ArrayHelper::map($roomsList, 'id', 'name'),['prompt' => 'Выберите номер...']) ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'check_in', [
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-calendar"></i>'
                        ]
                    ]
                ])->widget(\yii\jui\DatePicker::classname(), [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                        'minDate' => 1,
                    ],
            ])->textInput(['maxlength' => true, 'value' => $model->check_in]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'check_out', [
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-calendar"></i>'
                        ]
                    ]
                ])->widget(\yii\jui\DatePicker::classname(), [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                        'minDate' => 1,
                    ],
            ])->textInput(['maxlength' => true, 'value' => $model->check_out]) ?>
        </div>
    </div>    

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'adults')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'children')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        Booking::STATUS_NEW => BookingModule::t('backend', 'New'),
        Booking::STATUS_PROCESSED => BookingModule::t('backend', 'Processed'),
        Booking::STATUS_CANCELED => BookingModule::t('backend', 'Canceled'),
    ]) ?>

    <div class="well">
        <div class="pull-right">
            <?= Html::submitButton($model->getModel()->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Save'), ['class' => $model->getModel()->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

    <div id="calendar_all">
        <?php if($model->item_id and !isset($model->room_id)): ?>
            <?php
                $salerooms = Sale::find()->where(['category_id' => $model->item_id, 'status' => 1])->all(); 
                echo $this->render('calendar', [
                    'items' => $salerooms,
                ]);
            ?>
        <?php endif; ?>
    </div>
