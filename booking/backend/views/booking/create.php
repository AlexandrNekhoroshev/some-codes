<?php

use yii\helpers\Html;
use common\modules\booking\BookingModule;

/* @var $this yii\web\View */
/* @var $model common\modules\booking\common\models\BookingForm */

$this->title = BookingModule::t('backend', 'Create Booking');
$this->params['breadcrumbs'][] = ['label' => BookingModule::t('backend', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Create');
?>
<div class="booking-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
