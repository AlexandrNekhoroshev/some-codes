<?php foreach($items as $item): ?>
    <div class="row">
        <div class="col-md-3"><h4><?= $item->name ?></h4></div>
        <div class="col-md-9">
            <div id="calendar_<?= $item->id ?>"></div>                        
            <script>
                setTimeout(function(){
                    $(document).ready(function() {
                        $('#calendar_<?= $item->id ?>').eventCalendar({
                            eventsjson: '/admin/catalog/sale/json?room_id=<?= $item->id ?>',
                            showDayAsWeeks: false,
                            jsonDateFormat: 'human',
                            startWeekOnMonday: false,
                            openEventInNewWindow: true,
                            dateFormat: 'dddd DD-MM-YYYY',
                            showDescription: false,                                       
                        });
                    });
                }, 300);
            </script>
        </div>
    </div>
<?php endforeach; ?>