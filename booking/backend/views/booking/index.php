<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use common\modules\booking\BookingModule;
use common\modules\catalog\common\models\Item;
use common\modules\catalog\common\models\Sale;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\booking\backend\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = BookingModule::t('backend', 'Bookings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a('<i class="ion-plus-round"></i> ' . BookingModule::t('backend', 'Create Booking'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'data-table table-striped'],
        'layout' => "{items}\n{pager}\n{summary}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
            'attribute' => 'item_id',
            'value' => 'item.header',
            'filter' => Html::activeDropDownList($searchModel, 'item_id',
                ArrayHelper::map(Item::find()->active()->all(), 'id', 'header'),
                ['class'=>'form-control', 'prompt' => '']),
            ],
            [
            'attribute' => 'room_id',
            'value' => 'room.name',
            'filter' => Html::activeDropDownList($searchModel, 'room_id',
                ArrayHelper::map(Sale::find()->where(['status' => 1])->all(), 'id', 'name'),
                ['class'=>'form-control', 'prompt' => '']),
            ],            
            'email:email',
            'phone',
            [
                'attribute' => 'check_in',
                'format' => ['date', 'php:d/m/Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'check_in',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'minDate' => 1,
                    ],
                    'options' => ['placeholder' => 'Выбрать дату', 'size' => 10],
                ]),
            ],
            [
                'attribute' => 'check_out',
                'format' => ['date', 'php:d/m/Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'check_out',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'minDate' => 1,
                    ],
                    'options' => ['placeholder' => 'Выбрать дату', 'size' => 10],
                ]),
            ],            
            /*[
                'class' => 'backend\grid\SwitchStatusColumn',
                'checked' => function($model) {
                    return $model->status;
                }
            ],*/
            [
                'attribute' => 'status',
                'value' => function($data){
                    if($data->status == 1){
                        return '<i class="glyphicon glyphicon-ok" style="color:#5cb85c"></i>';
                    } elseif($data->status == 2) {
                        return '<i class="glyphicon glyphicon-ok-sign" style="color:#0e8eab"></i>';
                    } else {
                        return '<i class="glyphicon glyphicon-remove" style="color:#c9302c"></i>';
                    }                
                },
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status',
                    ['0' => 'Закрытые', '1' => 'Новые', '2' => 'Обработанные'],
                    ['class'=>'form-control', 'prompt' => '']),
            ],                        
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d/m/Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'minDate' => 1,
                    ],
                    'options' => ['placeholder' => 'Выбрать дату', 'size' => 10],
                ]),
            ],            
            ['class' => 'backend\grid\ActionColumn'],
        ],
    ]); ?>

</div>
