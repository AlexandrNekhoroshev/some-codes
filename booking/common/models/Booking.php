<?php

namespace common\modules\booking\common\models;

use Yii;
use common\modules\catalog\common\models\Item;
use common\modules\catalog\common\models\Sale;
use common\modules\booking\BookingModule;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%booking}}".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $room_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $comment
 * @property integer $check_in
 * @property integer $check_out
 * @property integer $adults
 * @property integer $children
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Item $item
 */
class Booking extends \yii\db\ActiveRecord
{
    const STATUS_CANCELED = 0;
    const STATUS_NEW = 1;
    const STATUS_PROCESSED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }  

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => BookingModule::t('common', 'Item'),
            'room_id' => BookingModule::t('common', 'Room'),
            'name' => BookingModule::t('common', 'Name'),
            'email' => BookingModule::t('common', 'Email'),
            'phone' => BookingModule::t('common', 'Phone'),
            'comment' => BookingModule::t('common', 'Additional'),
            'check_in' => BookingModule::t('common', 'Check In'),
            'check_out' => BookingModule::t('common', 'Check Out'),
            'adults' => BookingModule::t('common', 'Adults'),
            'children' => BookingModule::t('common', 'Children'),
            'status' => BookingModule::t('common', 'Status'),
            'created_at' => BookingModule::t('common', 'Created At'),
            'updated_at' => BookingModule::t('common', 'Updated At'),
            'created_by' => BookingModule::t('common', 'Created By'),
            'updated_by' => BookingModule::t('common', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\modules\booking\common\models\query\BookingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\BookingQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Sale::className(), ['id' => 'room_id']);
    } 

    public function getPricePeriod($check_in, $check_out, $rooms_id){
        $check_in = date_create($check_in);
        $check_out = date_create($check_out);
        $interval = date_diff($check_out, $check_in);
        $period = $interval->format('%a');
        $price = 0;
        for($i = 1; $i <= $period; $i++){
            date_add($check_in, date_interval_create_from_date_string('1 days'));
            $day = date_format($check_in, 'Y-m-d');             
            $sql = "SELECT price_table.price FROM price_table 
                    LEFT JOIN price ON '".$day."' BETWEEN price.date1 AND price.date2
                    WHERE price_table.period_id=price.id AND price_table.rooms_id=".$rooms_id.""; 
            $query = Yii::$app->db->createCommand($sql)->queryAll();
            if(!empty($query)){
                $price += $query[0]["price"];
            }
        }
        
        return $price;
    }
}
