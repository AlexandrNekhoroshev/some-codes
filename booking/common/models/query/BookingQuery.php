<?php

namespace common\modules\booking\common\models\query;

use common\modules\booking\common\models\Booking;

/**
 * This is the ActiveQuery class for [[\common\modules\booking\models\Booking]].
 *
 * @see \common\modules\booking\models\Booking
 */
class BookingQuery extends \yii\db\ActiveQuery
{

    public function active()
    {
        $this->andWhere(['status' => Booking::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status' => Booking::STATUS_INACTIVE]);
        return $this;
    }
}
