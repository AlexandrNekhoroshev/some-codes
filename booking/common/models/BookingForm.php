<?php

namespace common\modules\booking\common\models;

use common\modules\booking\BookingModule;
use common\modules\catalog\common\models\Item;
use Yii;
use yii\base\Model;
use common\models\ConfigItem;

class BookingForm extends Model
{
    public $item_id;
    public $room_id;
    public $email;
    public $phone;
    public $name;
    public $check_in;
    public $check_out;
    public $adults;
    public $children;
    public $additional_place;
    public $comment;
    public $status;
    public $full_name;
    public $rooms;
    public $created_at;
    public $updated_at;
    public $created_by;
    public $updated_by;
    public $vozrast;
    public $i_agree;

    private $model;

    public $subject = 'Бронь с сайта';

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => BookingModule::t('common', 'Item'),
            'room_id' => BookingModule::t('common', 'Room Id'),
            'name' => BookingModule::t('common', 'Name'),
            'email' => BookingModule::t('common', 'E-mail'),
            'phone' => BookingModule::t('common', 'Phone'),
            'check_in' => BookingModule::t('common', 'Check-in'),
            'check_out' => BookingModule::t('common', 'Check-out'),
            'adults' => BookingModule::t('common', 'Adults'),
            'children' => BookingModule::t('common', 'Children'),
            'status' => BookingModule::t('common', 'Status'),
            'rooms' => BookingModule::t('common', 'Номер'),
            'comment' => BookingModule::t('common', 'Comment'),
            'vozrast' => BookingModule::t('common', 'Возраст детей'),
            'created_at' => BookingModule::t('common', 'Created At'),
            'updated_at' => BookingModule::t('common', 'Updated At'),
            'created_by' => BookingModule::t('common', 'Created By'),
            'updated_by' => BookingModule::t('common', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id',  'adults', 'children',
                'status', 'created_at', 'updated_at', 'created_by',
                'updated_by','room_id'], 'integer'],
            [['item_id', 'email','name','phone'], 'required'],
            [['email'], 'email'],
            [['email', 'phone', 'name', 'rooms'], 'string', 'max' => 255],
            [['comment', 'check_in', 'check_out'], 'string'],
            [['i_agree',], 'boolean'],
            ['i_agree', 'compare', 'compareValue' => 1, 'message' => 'Выствите чебокс, иначе форма не отправится!'],
        ];
    }

    public static function findItems()
    {
        return Item::findAllActive();
    }

    public function setModel(Booking $model)
    {
        $this->model = $model;
        $this->attributes = [
            'item_id' => $model->item_id,
            'room_id' => $model->room_id,
            'name' => $model->name,
            'email' => $model->email,
            'phone' => $model->phone,
            'check_in' => $model->check_in,
            'check_out' => $model->check_out,
            'adults' => $model->adults,
            'children' => $model->children,
            'rooms' => $model->rooms,
//            'additional_place' => $model->additional_place,
            'comment' => $model->comment,
            'status' => $model->status,
        ];
    }

    public function getModel()
    {
        if (!$this->model) {
            $this->model = new Booking();
        }
        return $this->model;
    }

    public function saveModel()
    {
        if($this->validate()) {
            $model = $this->getModel();
            $model->setAttributes([
                'item_id' => $this->item_id,
                'room_id' => (isset($this->room_id)?$this->room_id:'0'),
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'check_in' => $this->check_in,
                'check_out' => $this->check_out,
                'adults' => $this->adults,
                'children' => $this->children,
                'rooms' => $this->rooms,
//                'additional_place' => $this->additional_place,
                'comment' => $this->comment,
                'status' => (isset($this->status)?$this->status:'1'),
            ], false);
            if($model->save()) {
                return $model;
            }
        }

        return null;


    }

    public function sendEmail()
    {
        
        $message = 'ФИО: ' . $this->name . PHP_EOL;
        $message .= 'E-mail: ' . $this->email . PHP_EOL;
        $message .= 'Телефон: ' . $this->phone . PHP_EOL;
        $message .= 'Дата заезда: ' . $this->check_in . PHP_EOL;
        $message .= 'Дата выезда: ' . $this->check_out . PHP_EOL;
        $message .= 'Взрослых: ' . $this->adults . PHP_EOL;
        $message .= 'Детей: ' . $this->children . PHP_EOL;
        $message .= 'Номер: ' . $this->rooms . PHP_EOL;
        $message .= 'Сообщение: ' . $this->comment . PHP_EOL;
        
        return Yii::$app->mailer->compose()
            ->setTo(ConfigItem::bookingEmail())
            ->setFrom('noreplay@magic-hotel.sprava1.com', "magic-hotel.sprava1.com")
            ->setSubject($this->subject)
            ->setTextBody($message)
            ->send();
    }
}