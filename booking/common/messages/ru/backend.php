<?php

return [
    'Booking' => 'Бронирование',
    'New' => 'Новый',
    'Processed' => 'Обработан',
    'Canceled' => 'Отменен',
    'Bookings' => 'Заявки',
    'Create Booking' => 'Создать заявку',
    'Update booking from: {name}' => 'Редактировать заявку от: {name}',
];