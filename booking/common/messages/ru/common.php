<?php

return [
    'Item' => 'Категория номера',
    'Room Id' => 'Номер',
    'Name' => 'Имя',
    'Room' => 'Комната',
    'Phone' => 'Телефон',
    'Additional' => 'Дополнительно',
    'Check In' => 'Дата заезда',
    'Check-in' => 'Дата заезда',
    'Check Out' => 'Дата выезда',
    'Check-out' => 'Дата выезда',
    'Adults' => 'Взрослых',
    'Children' => 'Детей',
    'Status' => 'Статус',
    'Comment' => 'Сообщение',
    'Created At' => 'Дата поступления',
    'Updated At' => 'Дата изменения',
    'Created By' => 'Создтель',
    'Updated By' => 'Редкатор',
];